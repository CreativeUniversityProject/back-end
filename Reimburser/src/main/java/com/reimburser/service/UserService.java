package com.reimburser.service;

import java.util.List;

import com.reimburser.model.User;



public interface UserService {
    public User getUser(Integer id);
    public List<User> findAll();
    public User addUser(User user);
    public User updateUser(User user);
    public void deleteUser(Integer id);
    //public User addUserRoletoUser(User user);
}
