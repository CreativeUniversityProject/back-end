package com.reimburser.service.servicelmpl;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.reimburser.model.Employee;
import com.reimburser.model.User;

import com.reimburser.repository.EmployeeRepository;
import com.reimburser.repository.UserRepository;
import com.reimburser.repository.UserRoleRepository;
import com.reimburser.service.UserService;

import javax.validation.ValidationException;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private EmployeeRepository empRepo;
    
    @Autowired
    private UserRoleRepository userRepo;

    @Override
    public User getUser(Integer id) {
        return userRepository
                .findById(id)
                .get();
    }

    @Override
    public List<User> findAll() {
        var users = userRepository.findAll();
        
        var result = new ArrayList<User>();
        users.forEach(e -> result.add(e));

        return result;
    }
    
    @Override
    public User addUser(User user) {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new ValidationException("Email exists!");
        }
        user.setPassword(passwordEncoder.encode("test"));
 
		 Employee employee=empRepo.findById(user.getEmployee().getEmployeeId()).get();
			 user.setEmployee(employee);
//			 
////			 user.getUserRole()
////				.addAll(user.getUserRole().stream().map(
////						role->{
////							UserRole userRole=userRepo.findById(role.getRoleId()).get();
////						userRole.getUser().add(user);
////							return userRole;
////							
////						}).collect(Collectors.toList()));
			 user.setUserRole(user.getUserRole());
		
         return userRepository.save(user);
       
    }
    
//    @Override
//    public User addUserRoletoUser(User user) {
//    	//User user=userRepo.findById();
//			 user.getUserRole()
//				.addAll(user.getUserRole().stream().map(
//						role->{
//							UserRole userRole=userRepo.findById(role.getRoleId()).get();
//						userRole.getUser().add(user);
//							return userRole;
//							
//						}).collect(Collectors.toList()));
//		
//         return userRepository.save(user);
//       
//    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
    
    

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       //User user=userRepository.findByEmail(username).get();
    	return userRepository
                .findByEmail(username)
                .get();
    }
    
//    private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
//        String[] userRoles = user.getUserRole().stream().map((role) -> role.getRoleName()).toArray(String[]::new);
//        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
//        return authorities;
//    }

}
