package com.reimburser.controller;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.dto.EmailDto;

@RestController
@CrossOrigin
public class EmailController {
	
	@Autowired
	private JavaMailSender mailSender;
	
	@PostMapping("/email")
	public EmailDto sendEmail(@RequestBody EmailDto emailDto) throws MessagingException {
		String from = "hexclanuom@gmail.com";
		String to = emailDto.getReceiverEmail();
		 
//		SimpleMailMessage message = new SimpleMailMessage();
//		 
//		message.setFrom(from);
//		message.setTo(to);
//		message.setSubject(emailDto.getSubject());
//		message.setText(emailDto.getText());
		 
		
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		 
		helper.setSubject(emailDto.getSubject());
		helper.setFrom(from);
		helper.setTo(to);
		 
		boolean html = true;
		helper.setText("<b>Hi</b>,<br><i>"+emailDto.getText()+"</i>", html);
		mailSender.send(message);
		return emailDto;
	}

}
