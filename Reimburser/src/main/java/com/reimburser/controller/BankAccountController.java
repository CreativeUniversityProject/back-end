package com.reimburser.controller;


import java.util.List;
import java.util.Optional;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.BankAccount;
import com.reimburser.model.Employee;
import com.reimburser.repository.BankAccountRepository;
import com.reimburser.repository.EmployeeRepository;

@RestController
@CrossOrigin
public class BankAccountController {
	
	@Autowired
	BankAccountRepository repo;
	
	@Autowired
	EmployeeRepository empRepo;
	
	@GetMapping("/allBankAccounts") 
	public List<BankAccount> getAllBankAccounts() {
		return repo.findAll();
	}
	@GetMapping("/BankAccount/{id}")
	public Optional<BankAccount> getBankAccount(@PathVariable Integer id) {	
		return repo.findById(id);
	}
	
	@GetMapping("/employeeBankAccount/{eid}")
	public BankAccount getBankAccountForEmployee(@PathVariable int eid) {	
		Employee employee=empRepo.findById(eid).get();
		return repo.findByEmployee(employee);
	}
	
	@PostMapping("/addBankAccount")
	public BankAccount addBankAccount(@Valid @RequestBody BankAccount bankAccount) {

		Employee employee=empRepo.findById(bankAccount.getEmployee().getEmployeeId()).get();
		bankAccount.setEmployee(employee);
		 repo.save(bankAccount);
		 return bankAccount;
	}
	
	@PutMapping("/updateBankAccount/{id}")
	ResponseEntity<BankAccount> updateBankAccount(@Valid @RequestBody BankAccount bankAccount){

		Employee employee=empRepo.findById(bankAccount.getEmployee().getEmployeeId()).get();
		bankAccount.setEmployee(employee);
		BankAccount result= repo.save(bankAccount);
		return ResponseEntity.ok().body(result);
	}
	
	@PutMapping("/updateBankAccountEmployee/{id}")
	ResponseEntity<BankAccount> updateBankAccountEmployee(@Valid @RequestBody BankAccount bankAccount, @PathVariable Integer id){
		BankAccount currentAccount=repo.findById(id).get();
		currentAccount.setAccountNo(bankAccount.getAccountNo());
		currentAccount.setBankName(bankAccount.getBankName());
		currentAccount.setBranch(bankAccount.getBranch());
		Employee employee=empRepo.findById(currentAccount.getEmployee().getEmployeeId()).get();
		currentAccount.setEmployee(employee);
		BankAccount result= repo.save(currentAccount);
		return ResponseEntity.ok().body(result);
	}
	
	
	@DeleteMapping("/deleteBankAccount/{id}")
	ResponseEntity<BankAccount> deleteProject(@PathVariable Integer id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
	
	

}
