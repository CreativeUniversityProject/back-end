package com.reimburser.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api")
public class AppController {
    
    @GetMapping
    public String home() {
        return "Rest API Security Demo";
    }
}
