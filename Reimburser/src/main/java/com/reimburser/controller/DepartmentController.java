package com.reimburser.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.Department;
import com.reimburser.repository.DepartmentRepository;

@CrossOrigin
@RestController
public class DepartmentController {

	@Autowired
	DepartmentRepository repo;
	
	@GetMapping("/allDepartment")
	public List<Department> getAllDepartments() {
		return repo.findAll();
	}
	@GetMapping("/Department/{id}")
	public Optional<Department> getDepartment(@PathVariable int id) {
		return repo.findById(id);
	}
	@PostMapping("/addDepartment")
	public Department addDepartment(@Valid @RequestBody Department department) {
		 repo.save(department);
		 return department;
	}
	
	@PutMapping("/updateDepartment/{id}")
	ResponseEntity<Department> updateDepartment(@Valid @RequestBody Department department){
		Department result= repo.save(department);
		return ResponseEntity.ok().body(result);
	}
	@DeleteMapping("/deleteDepartment/{id}")
	ResponseEntity<Department> deleteDepartment(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
	
}
