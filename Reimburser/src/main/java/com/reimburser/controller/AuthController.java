package com.reimburser.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.config.JwtTokenUtil;
import com.reimburser.model.User;
import com.reimburser.model.dto.PasswordDto;
import com.reimburser.repository.UserRepository;
import com.reimburser.service.UserService;

@RestController
@CrossOrigin
@RequestMapping(path="/api/auth")
public class AuthController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UserRepository repo;
    
    
    @PostMapping("/signIn")
    public ResponseEntity<User> login(@RequestBody User request) {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            User user = (User) authenticate.getPrincipal();
            	//user.setRoles(user.getUserRole());
    	

            return ResponseEntity.ok()
                    .header( HttpHeaders.AUTHORIZATION, jwtTokenUtil.generateAccessToken(user))
                    .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS , HttpHeaders.AUTHORIZATION)
                    .body(user);
            
      
        } catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
    @PostMapping("/changePassword")
    public ResponseEntity<User> changePassword(@RequestBody PasswordDto request) {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(request.getUserName(), request.getOldPassword()));

            User user = (User) authenticate.getPrincipal();
            user.setPassword(passwordEncoder.encode(request.getNewPassword()));
            repo.save(user);
            return ResponseEntity.ok().body(user);
            
      
        } catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
    
    

    @PostMapping("/signUp")
    public User register(@RequestBody User request) {
        return userService.addUser(request);
    }
}
