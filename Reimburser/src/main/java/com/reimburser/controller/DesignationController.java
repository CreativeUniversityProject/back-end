package com.reimburser.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.reimburser.model.Designation;
import com.reimburser.repository.DesignationRepository;

@CrossOrigin
@RestController
public class DesignationController {
	
	@Autowired
	DesignationRepository repo;
	
	@GetMapping("/allDesignation")
	public List<Designation> getAllDesignations() {
		// Thread.sleep(3000);
		return repo.findAll();
	}
	@GetMapping("/Designation/{id}")
	public Optional<Designation> getDesignation(@PathVariable int id) {
		// Thread.sleep(3000);
		return repo.findById(id);
	}
	@PostMapping("/addDesignation")
	public Designation addDesignation(@Valid @RequestBody Designation designation) {
		// Thread.sleep(3000);
		 repo.save(designation);
		 return designation;
	}
	
	@PutMapping("/updateDesignation/{id}")
	ResponseEntity<Designation> updateDesignation(@Valid @RequestBody Designation designation){
		Designation result= repo.save(designation);
		return ResponseEntity.ok().body(result);
	}
	@DeleteMapping("/deleteDesignation/{id}")
	ResponseEntity<Designation> deleteDesignation(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}

}
