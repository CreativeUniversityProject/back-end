package com.reimburser.controller;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.Employee;
import com.reimburser.model.Project;
import com.reimburser.repository.EmployeeRepository;
import com.reimburser.repository.ProjectRepository;

@RestController
@CrossOrigin
public class ProjectController {
	@Autowired
	ProjectRepository repo;
	
	
	@Autowired
	EmployeeRepository empRepo;
	
	@GetMapping("/allProjects")
	public List<Project> getAllProjects() {
		return repo.findAll();
	}
	@GetMapping("/Project/{id}")
	public Optional<Project> getProject(@PathVariable int id) {	
		return repo.findById(id);
	}
	
	@GetMapping("/employeeProjects/{eid}")
	public Collection<Project> getEmployeeProjects(@PathVariable int eid) {	
		Employee employee=empRepo.findById(eid).get();
		return repo.findByEmployee(employee);
	}
	
	@PostMapping("/addProject")
	public Project addProject(@Valid @RequestBody Project project) {
		// Thread.sleep(3000);
		
				project.getEmployee()
				.addAll(project.getEmployee().stream().map(
						emp->{
							Employee employee=empRepo.findById(emp.getEmployeeId()).get();
						employee.getProject().add(project);
							return employee;
							
						}).collect(Collectors.toList()));
		
		 repo.save(project);
		 return project;
	}
	
	@PutMapping("/updateProject/{id}")
	ResponseEntity<Project> updateProject(@Valid @RequestBody Project project, @PathVariable int id){
		
		Project currentProject=repo.findById(id).get();
		currentProject.setEndDate(project.getEndDate());
		project.getEmployee()
		.addAll(project.getEmployee().stream().map(
				emp->{
					Employee employee=empRepo.findById(emp.getEmployeeId()).get();
				employee.getProject().add(project);
					return employee;
				}).collect(Collectors.toList()));

		Project result= repo.save(project);
		return ResponseEntity.ok().body(result);
	}
	
	@DeleteMapping("/deleteProject/{id}")
	ResponseEntity<Project> deleteProject(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
	

}
