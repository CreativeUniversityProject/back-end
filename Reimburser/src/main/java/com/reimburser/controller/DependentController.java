package com.reimburser.controller;
import java.util.Collection;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.reimburser.model.Dependent;
import com.reimburser.model.Employee;
import com.reimburser.repository.Dependentrepository;
import com.reimburser.repository.EmployeeRepository;

@CrossOrigin
@RestController
public class DependentController {

	@Autowired
	Dependentrepository repo;
	
	@Autowired
	EmployeeRepository empRepo;
	
	@GetMapping("/dependent")
	public List<Dependent> getAllDependents() {
		return repo.findAll();
	}
	
	@GetMapping("/dependent/{id}")
	public Dependent getDependent(@PathVariable int id) {
		return repo.findById(id).get();
	}
	
	@GetMapping("/dependentForEmployee/{eid}")
	public Collection<Dependent> getDependentForEmployee(@PathVariable int eid) {
		Employee employee=empRepo.findById(eid).get();
		return repo.findByEmployee(employee);
	}
	
	@PostMapping("/admin/addDependent")
	public Dependent addDependent(@Valid @RequestBody Dependent dependent) {	
			 Employee employee=empRepo.findById(dependent.getEmployee().getEmployeeId()).get();
			 dependent.setEmployee(employee);
		 repo.save(dependent);
		 return dependent;
	}
	
	@PutMapping("/updateDependent/{id}")
	ResponseEntity<Dependent> updateDependent(@Valid @RequestBody Dependent dependent){
		 
			 Employee employee=empRepo.findById(dependent.getEmployee().getEmployeeId()).get();
			 dependent.setEmployee(employee);
			
		Dependent result= repo.save(dependent);
		return ResponseEntity.ok().body(result);
	}
	@DeleteMapping("/admin/deleteDependent/{id}")
	ResponseEntity<Dependent> deleteDependent(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
	
	
}
