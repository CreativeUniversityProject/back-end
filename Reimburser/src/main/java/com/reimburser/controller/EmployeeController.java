package com.reimburser.controller;
import java.util.Collection;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.Department;
import com.reimburser.model.Designation;
import com.reimburser.model.Employee;
import com.reimburser.model.User;
import com.reimburser.model.UserRole;

import com.reimburser.repository.DepartmentRepository;
import com.reimburser.repository.DesignationRepository;
import com.reimburser.repository.EmployeeRepository;
import com.reimburser.repository.UserRepository;
import com.reimburser.repository.UserRoleRepository;


//import com.reimburser.repository.EmployeeRepository;

@CrossOrigin
@RestController
@RequestMapping
public class EmployeeController {


	
	@Autowired
	EmployeeRepository repo;
	
	@Autowired
	DepartmentRepository depRepo;
	
	@Autowired
	DesignationRepository desRepo;
	
	@Autowired
	UserRoleRepository userRepo;
	
	@Autowired
	UserRepository useRepo;
	
	

    
    @Autowired
    private PasswordEncoder passwordEncoder;
	

	@GetMapping("/employee")
	public Collection<Employee> getAllEmployees() {
		 Collection<Employee> employees=repo.findAll();
		 for (Employee employee : employees) {
			 User user=useRepo.findByEmployee(employee);
				UserRole userRole=user.getUserRole();
				employee.setUserRole(userRole);
		    }
		return employees;
	}
	
	@GetMapping("/employee/{id}")
	public Employee getEmployee(@PathVariable int id) {		
		Employee employee=repo.findById(id).get();
		User user=useRepo.findByEmployee(repo.findById(id).get());
		UserRole userRole=user.getUserRole();
		employee.setUserRole(userRole);
		return employee;
	}
	
//	@GetMapping("/employeeRole/{id}")
//	public Collection<UserRole> getEmployeeRole(@PathVariable int id) {		
//		User user=useRepo.findByEmployee(repo.findById(id).get());
//		Collection<UserRole> userRole=user.getUserRole();
//		return userRole;
//	}

	@PostMapping("admin/addemployee")
	public Employee addEmployee(@Valid @RequestBody Employee employee) {
		//Department
		Department dep = depRepo.findById(employee.getDepartment().getDepartmentId()).get();
		employee.setDepartment(dep);
		//Designation
		Designation des = desRepo.findById(employee.getDesignation().getDesignationId()).get();
		employee.setDesignation(des);
		User newUser=new User();
		
		UserRole userRole=employee.getUserRole();
	         
			Employee emp= repo.save(employee);
	         newUser.setEmail(emp.getEmail());
				newUser.setEmployee(emp);
				
				newUser.setUserRole(userRole);
				
				User user=addUser(newUser);
				
				useRepo.save(user);
				//ResponseEntity.ok().body(emp);
	         
		 return emp;
	}
	
	 public User addUser(User user) {
	        if (useRepo.findByEmail(user.getEmail()).isPresent()) {
	            throw new ValidationException("Email exists!");
	        }
	        user.setPassword(passwordEncoder.encode("test"));
	 
//				 Employee employee=repo.findById(user.getEmployee().getEmployeeId()).get();
//				 user.setEmployee(employee);
				 
//				user.getUserRole();
//				UserRole userRole=userRepo.findById(role.getRoleId()).get();
//				userRole.getUser().add(user);
	   	 user.setUserRole(user.getUserRole());
							
							
			
	         return user;
	    }
	
	@PutMapping("/admin/updateEmployee")
	ResponseEntity<Employee> updateEmployee(@Valid @RequestBody Employee employee){
		
		Department dep = depRepo.findById(employee.getDepartment().getDepartmentId()).get();
		employee.setDepartment(dep);
		
		Designation des = desRepo.findById(employee.getDesignation().getDesignationId()).get();
		employee.setDesignation(des);
		//userRole
		User user=useRepo.findByEmployee(employee);
		UserRole role=employee.getUserRole();
		user.setUserRole(role);
	
		Employee result= repo.save(employee);
		
		return ResponseEntity.ok().body(result);
	}
	
//	 public ResponseEntity<User> updateUserRole (@RequestBody User user,@PathVariable int uid) {
//	    	User result=useRepo.findById(uid).get();
//	    	result.setUserRole(user.getUserRole());
//	    	
//	        return ResponseEntity.ok().body(result);  
//	    }
//	
	@PutMapping("/updateEmployeeProfile/{eid}")
	User updateEmployeeProfile(@Valid @RequestBody Employee employee, @PathVariable int eid){
		
		Employee updatableEmployee=repo.findById(eid).get();
		updatableEmployee.setAddress(employee.getAddress());
		updatableEmployee.setPhoneNo(employee.getPhoneNo());
		updatableEmployee.setFirstName(employee.getFirstName());
		updatableEmployee.setLastName(employee.getLastName());
		
		Employee result= repo.save(updatableEmployee);
		
		 ResponseEntity.ok().body(result);
		 User user=useRepo.findByEmployee(updatableEmployee);
		 return user;
		 
	}
	
	@DeleteMapping("/admin/deleteEmployee/{id}")
	ResponseEntity<Employee> deleteEmployee(@PathVariable int id){
		Employee employee=repo.findById(id).get();
	User user=useRepo.findByEmployee(employee);

	repo.deleteById(id);
	useRepo.deleteById(user.getId());
	return ResponseEntity.ok().build();
	}
	
}
