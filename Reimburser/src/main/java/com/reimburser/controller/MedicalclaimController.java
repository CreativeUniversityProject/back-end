package com.reimburser.controller;


import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.reimburser.model.Dependent;
import com.reimburser.model.Employee;
import com.reimburser.model.MedicalClaim;
import com.reimburser.repository.BankAccountRepository;
import com.reimburser.repository.Dependentrepository;
import com.reimburser.repository.EmployeeRepository;
import com.reimburser.repository.MedicalRepository;
import com.reimburser.repository.ReceiptRepository;

@CrossOrigin
@RestController
public class MedicalclaimController {
	@Autowired
	MedicalRepository repo;
	
	@Autowired
	EmployeeRepository empRepo;
	
	@Autowired
	ReceiptRepository recRepo;
	
	@Autowired
	Dependentrepository depRepo;
	
	@Autowired
	BankAccountRepository bankRepo;
	
	@GetMapping("/allMedicalClaims")
	public List<MedicalClaim> getAllMedicalClaims() {
		return repo.findAll();
	}
	@GetMapping("/MedicalClaims/{id}")
	public Optional<MedicalClaim> getMedicalClaim(@PathVariable int id) {
		return repo.findById(id);
	}
	@GetMapping("/MedicalClaimForEmployee/{eid}")
	public Collection<MedicalClaim> getMedicalClaimForEmployee(@PathVariable int eid) {
		Collection<MedicalClaim> medicalClaim=repo.findByEmployee((empRepo.findById(eid)).get());
		return medicalClaim;
	}
	@GetMapping("/MedicalClaimFortechlead/{tid}")
	public Collection<MedicalClaim> getMedicalClaimForTechlead(@PathVariable int tid) {
		List<MedicalClaim> medicalClaim=repo.findByTechleadId(tid);
		return medicalClaim;
	}
	
	@GetMapping("/MedicalClaimForStatus/{status}")
	public Collection<MedicalClaim> getMedicalClaimForStatus(@PathVariable String status) {
		List<MedicalClaim> medicalClaim=repo.findByProgressStatus(status);
		return medicalClaim;
	}
	@GetMapping("/MedicalClaimForStatuses/{status1}/{status2}/{status3}")
	public Collection<MedicalClaim> getMedicalClaimForStatuses(@PathVariable String status1,@PathVariable String status2,@PathVariable String status3) {
		List<MedicalClaim> medicalClaim=repo.findByProgressStatus(status1);
		medicalClaim.addAll(repo.findByProgressStatus(status2));
		medicalClaim.addAll(repo.findByProgressStatus(status3));
		return medicalClaim;
	}
	@GetMapping("/MedicalClaimForStatuses2/{status1}/{status2}")
	public Collection<MedicalClaim> getMedicalClaimForStatuses(@PathVariable String status1,@PathVariable String status2) {
		List<MedicalClaim> medicalClaim=repo.findByProgressStatus(status1);
		medicalClaim.addAll(repo.findByProgressStatus(status2));
		return medicalClaim;
	}
	
	@PostMapping("/addMedicalClaim")
	public MedicalClaim addMedicalClaim(@Valid @RequestBody MedicalClaim medicalClaim) {
		 try {
			 Employee employee=empRepo.findById(medicalClaim.getEmployee().getEmployeeId()).get();
			 medicalClaim.setEmployee(employee);
			 } catch (Exception e) {
			
				 System.out.println("not found");
				 
			 };
				medicalClaim.getReceipt()
				.addAll(medicalClaim.getReceipt().stream().map(
						rec->{
							rec.setMedicalClaim(medicalClaim);
							recRepo.save(rec);
							return rec;
							
						}).collect(Collectors.toList()));
		
				medicalClaim.getDependent()
				.addAll(medicalClaim.getDependent().stream().map(
						dep->{
							Dependent dependent=depRepo.findById(dep.getDependentId()).get();
						dependent.getMedicalClaim().add(medicalClaim);
							return dependent;
							
						}).collect(Collectors.toList()));
				
//				BankAccount bankAccount=bankRepo.findById(medicalClaim.getBankAccount().getAccountNo()).get();
//				medicalClaim.setBankAccount(bankAccount);
//		
		 repo.save(medicalClaim);
		 return medicalClaim;
	}
	
	@PutMapping("/updateMedicalClaim/{id}")
	ResponseEntity<MedicalClaim> updateMedicalClaim(@Valid @RequestBody MedicalClaim medicalClaim){
		 try {
			 Employee employee=empRepo.findById(medicalClaim.getEmployee().getEmployeeId()).get();
			 medicalClaim.setEmployee(employee);
			 } catch (Exception e) {
			
				 System.out.println("not found");
				 
			 };
				medicalClaim.getReceipt()
				.addAll(medicalClaim.getReceipt().stream().map(
						rec->{
							rec.setMedicalClaim(medicalClaim);
							recRepo.save(rec);
							return rec;
							
						}).collect(Collectors.toList()));
				
//				BankAccount bankAccount=bankRepo.findById(medicalClaim.getBankAccount().getAccountNo()).get();
//				medicalClaim.setBankAccount(bankAccount);
				
				
		MedicalClaim result= repo.save(medicalClaim);
		return ResponseEntity.ok().body(result);
	}
	
	@PutMapping("/updateMedicalClaimStatus/{mid}")
	ResponseEntity<MedicalClaim> updateMedicalClaimStatus(@Valid @RequestBody MedicalClaim medicalClaim,@PathVariable int mid){
		
		MedicalClaim medicalClaim1= repo.findById(mid).get();
		medicalClaim1.setProgressStatus(medicalClaim.getProgressStatus());
		MedicalClaim result=repo.save(medicalClaim1);
		return ResponseEntity.ok().body(result);
	}
	
	
	@DeleteMapping("/deleteMedicalClaim/{id}")
	ResponseEntity<MedicalClaim> deleteMedicalClaim(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
	
	
	

}
