package com.reimburser.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.UserRole;
import com.reimburser.repository.UserRoleRepository;

@RestController
@CrossOrigin
public class UserRoleController {

	@Autowired
	UserRoleRepository repo;
	
	@GetMapping("/allRoles")
	public List<UserRole> getAllRoles() {
		return repo.findAll();
	}
	@GetMapping("/Roles/{id}")
	public Optional<UserRole> getRole(@PathVariable int id) {
		
		return repo.findById(id);
	}
	@PostMapping("/addUserRole")
	public UserRole addUserRole(@Valid @RequestBody UserRole userRole) {
		// Thread.sleep(3000);
		 repo.save(userRole);
		 return userRole;
	}
	
	@PutMapping("/updateUserRole/{id}")
	ResponseEntity<UserRole> updateUserRole(@Valid @RequestBody UserRole userRole){
		UserRole result= repo.save(userRole);
		return ResponseEntity.ok().body(result);
	}
	@DeleteMapping("/deleteUserRole/{id}")
	ResponseEntity<UserRole> deleteUserRole(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
}
