package com.reimburser.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.Employee;
import com.reimburser.model.ProfilePictures;
import com.reimburser.repository.EmployeeRepository;
import com.reimburser.repository.ProfilePictureRepository;


@RestController
@CrossOrigin
public class ProfilePictureController {

	@Autowired
	ProfilePictureRepository repo;
	
	@Autowired
	EmployeeRepository empRepo;
	
	@GetMapping("/allProfilePictures")
	public List<ProfilePictures> getAllProfilePictures() {
		return repo.findAll();
	}
	@GetMapping("/ProfilePicture/{id}")
	public Optional<ProfilePictures> getRole(@PathVariable int id) {
		
		return repo.findById(id);
	}
	
	@GetMapping("/ProfilePictureOfEmployee/{eid}")
	public ProfilePictures getProfilePictureOfEmployee(@PathVariable int eid) {
		Employee employee=empRepo.findById(eid).get();
		ProfilePictures profilePicture=repo.findByEmployee(employee);
		return profilePicture;
	}
	
	
	@PostMapping("/addProfilePicture")
	public ProfilePictures addProfilePicture(@Valid @RequestBody ProfilePictures profilePicture) {
		// Thread.sleep(3000);
		Employee employee=empRepo.findById(profilePicture.getEmployee().getEmployeeId()).get();
		profilePicture.setEmployee(employee); 
		employee.setProfilePictures(profilePicture);
		repo.save(profilePicture);
		 return profilePicture;
	}
	
	@PutMapping("/updateProfilePicture/{id}")
	ResponseEntity<ProfilePictures> updateProfilePictures(@Valid @RequestBody ProfilePictures profilePicture){
		Employee employee=empRepo.findById(profilePicture.getEmployee().getEmployeeId()).get();
		profilePicture.setEmployee(employee); 
		employee.setProfilePictures(profilePicture);
		ProfilePictures result= repo.save(profilePicture);
		return ResponseEntity.ok().body(result);
	}
	@DeleteMapping("/deleteProfilePicture/{id}")
	ResponseEntity<ProfilePictures> deleteProfilePicture(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
}
