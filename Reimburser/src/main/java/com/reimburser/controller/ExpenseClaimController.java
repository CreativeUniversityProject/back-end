package com.reimburser.controller;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.Employee;
import com.reimburser.model.ExpenseClaim;
import com.reimburser.repository.EmployeeRepository;
import com.reimburser.repository.ExpenseRepository;
import com.reimburser.repository.ImageRepository;


@CrossOrigin
@RestController
public class ExpenseClaimController {
	@Autowired
	ExpenseRepository repo;
	
	@Autowired
	EmployeeRepository empRepo;
	
	@Autowired
	ImageRepository imgRepo;
	
	@GetMapping("/allExpenseClaims")
	public List<ExpenseClaim> getAllExpenseClaims() {
		return repo.findAll();
	}
	@GetMapping("/ExpenseClaims/{id}")
	public Optional<ExpenseClaim> getExpenseClaim(@PathVariable int id) {
		return repo.findById(id);
	}
	@GetMapping("/ExpenseClaimForEmployee/{eid}")
	public Collection<ExpenseClaim> getExpenseClaimForEmployee(@PathVariable int eid) {
		Collection<ExpenseClaim> expenseClaim=repo.findByEmployee((empRepo.findById(eid)).get());
		return expenseClaim;
	}
	
	@GetMapping("/ExpenseClaimFortechlead/{tid}")
	public Collection<ExpenseClaim> getExpenseClaimForTechlead(@PathVariable int tid) {
		List<ExpenseClaim> expenseClaim=repo.findByTechleadId(tid);
		return expenseClaim;
	}
	
	@GetMapping("/ExpenseClaimForStatus/{status}")
	public Collection<ExpenseClaim> getExpenseClaimForStatus(@PathVariable String status) {
		List<ExpenseClaim> expenseClaim=repo.findByProgressStatus(status);
		return expenseClaim;
	}
	@GetMapping("/ExpenseClaimForStatuses/{status1}/{status2}/{status3}")
	public Collection<ExpenseClaim> getExpenseClaimForStatuses(@PathVariable String status1,@PathVariable String status2,@PathVariable String status3) {
		List<ExpenseClaim> expenseClaim=repo.findByProgressStatus(status1);
		expenseClaim.addAll(repo.findByProgressStatus(status2));
		expenseClaim.addAll(repo.findByProgressStatus(status3));
		return expenseClaim;
	}
	@GetMapping("/ExpenseClaimForStatuses2/{status1}/{status2}")
	public Collection<ExpenseClaim> getMedicalClaimForStatuses(@PathVariable String status1,@PathVariable String status2) {
		List<ExpenseClaim> expenseClaim=repo.findByProgressStatus(status1);
		expenseClaim.addAll(repo.findByProgressStatus(status2));
		return expenseClaim;
	}
	
	
	@PostMapping("/addExpenseClaim")
	public ExpenseClaim addExpenseClaim(@Valid @RequestBody ExpenseClaim expenseClaim) {
		 try {
			 Employee employee=empRepo.findById(expenseClaim.getEmployee().getEmployeeId()).get();
			 expenseClaim.setEmployee(employee);
			 } catch (Exception e) {
			
				 System.out.println("not found");				 
			 };
			 
				expenseClaim.getImage()
				.addAll(expenseClaim.getImage().stream().map(
						img->{
							img.setExpenseClaim(expenseClaim);
							imgRepo.save(img);
							return img;
							
						}).collect(Collectors.toList()));	 
		
		 repo.save(expenseClaim);
		 return expenseClaim;
	}
	
	@PutMapping("/updateExpenseClaim/{id}")
	ResponseEntity<ExpenseClaim> updateExpenseClaim(@Valid @RequestBody ExpenseClaim expenseClaim){
		
		ExpenseClaim result= repo.save(expenseClaim);
		return ResponseEntity.ok().body(result);
	}
	
	@PutMapping("/updateExpenseClaimStatus/{mid}")
	ResponseEntity<ExpenseClaim> updateExpenseClaimStatus(@Valid @RequestBody ExpenseClaim expenseClaim,@PathVariable int mid){
		
		ExpenseClaim expenseClaim1= repo.findById(mid).get();
		expenseClaim1.setProgressStatus(expenseClaim.getProgressStatus());
		expenseClaim1.setApprovedAmount(expenseClaim.getApprovedAmount());
		expenseClaim1.setRemark(expenseClaim.getRemark());
		ExpenseClaim result=repo.save(expenseClaim1);
		return ResponseEntity.ok().body(result);
	}
	
	@DeleteMapping("/deleteExpenseClaim/{id}")
	ResponseEntity<ExpenseClaim> deleteMedicalClaim(@PathVariable int id){
	repo.deleteById(id);
	return ResponseEntity.ok().build();
	}
	
}
