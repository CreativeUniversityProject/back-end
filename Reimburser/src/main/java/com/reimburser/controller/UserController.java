package com.reimburser.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reimburser.model.Employee;
import com.reimburser.model.User;
import com.reimburser.model.UserRole;
import com.reimburser.repository.UserRepository;
import com.reimburser.repository.UserRoleRepository;
import com.reimburser.service.UserService;


@RestController
@CrossOrigin
@RequestMapping(path="/api/users")
public class UserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository repo;
    
    @Autowired
    private UserRoleRepository userRepo;
    
    @GetMapping
    public List<User> allUsers() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable String id) {
    	
         User user=userService.getUser(Integer.parseInt(id));
       
        return user;
    }
    
	@GetMapping("/employeeTechLeads/{uid}")
	public Collection<Employee> getTechLead(@PathVariable int uid) {	
		UserRole userRole=userRepo.findByRoleName("TECHLEAD");
		Collection<User> users=repo.findByUserRole(userRole);
		try {
			users.remove(repo.findById(uid).get());
		}
		catch(Exception e) {
			System.out.println("User is not a techlead");
		}
		
	Collection<Employee> employees= new ArrayList<>(); 
    
    for (User user : users) {
        employees.add(user.getEmployee());
    
    }
		return employees;
	}
	
	
	@GetMapping("/employeeAccountants/{uid}")
	public Collection<Employee> getAccountant(@PathVariable int uid) {	
		UserRole userRole=userRepo.findByRoleName("ACCOUNTANT");
		Collection<User> users=repo.findByUserRole(userRole);
		try {
			users.remove(repo.findById(uid).get());
		}
		catch(Exception e) {
			System.out.println("User is not a techlead");
		}
		
	Collection<Employee> employees= new ArrayList<>(); 
    
    for (User user : users) {
        employees.add(user.getEmployee());
    
    }
		return employees;
	}
    
    @PostMapping
    public User updateUser (@RequestBody User user) {
        return userService.updateUser(user);     
    }
    
    @PutMapping("/roleChange/{uid}")
    public ResponseEntity<User> updateUserRole (@RequestBody User user,@PathVariable int uid) {
    	User result=repo.findById(uid).get();
    	result.setUserRole(user.getUserRole());
    	
        return ResponseEntity.ok().body(result);   
    }

    
    
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable String id) {
        userService.deleteUser(Integer.parseInt(id));
    }
}
