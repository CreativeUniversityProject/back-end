package com.reimburser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReimburserApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReimburserApplication.class, args);
	}

}
