package com.reimburser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {

}
