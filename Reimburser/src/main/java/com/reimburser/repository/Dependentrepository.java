package com.reimburser.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.reimburser.model.Dependent;
import com.reimburser.model.Employee;

@Repository
public interface Dependentrepository extends JpaRepository<Dependent, Integer> {
	  
	public Collection<Dependent> findByEmployee(Employee employee);
	

}
