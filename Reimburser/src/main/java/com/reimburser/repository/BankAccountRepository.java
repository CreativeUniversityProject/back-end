package com.reimburser.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.reimburser.model.BankAccount;
import com.reimburser.model.Employee;

public interface BankAccountRepository extends JpaRepository<BankAccount, Integer> {
	public BankAccount findByEmployee(Employee employee);

}
