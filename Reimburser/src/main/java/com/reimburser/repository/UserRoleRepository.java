package com.reimburser.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.reimburser.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

	public UserRole findByRoleName(String name);
}
