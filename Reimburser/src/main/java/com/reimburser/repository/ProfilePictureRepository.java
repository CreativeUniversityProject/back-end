package com.reimburser.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.reimburser.model.Employee;
import com.reimburser.model.ProfilePictures;

public interface ProfilePictureRepository extends JpaRepository<ProfilePictures, Integer> {
	
	public ProfilePictures findByEmployee(Employee employee);

}
