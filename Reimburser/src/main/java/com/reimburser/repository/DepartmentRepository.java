package com.reimburser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {

}
