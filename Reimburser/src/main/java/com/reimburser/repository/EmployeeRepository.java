package com.reimburser.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Employee;


@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
//	public Collection<Employee> findByUserRole(UserRole userRole);
	

}
