package com.reimburser.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Employee;
import com.reimburser.model.MedicalClaim;

@Repository
public interface MedicalRepository extends JpaRepository<MedicalClaim, Integer>{
	
	public Collection<MedicalClaim> findByEmployee(Employee employee);
	
	public List<MedicalClaim> findByTechleadId(int id);
	
	public List<MedicalClaim> findByProgressStatus(String status);

}
