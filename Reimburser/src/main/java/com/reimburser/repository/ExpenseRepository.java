package com.reimburser.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Employee;
import com.reimburser.model.ExpenseClaim;


@Repository
public interface ExpenseRepository extends JpaRepository<ExpenseClaim, Integer> {
	public Collection<ExpenseClaim> findByEmployee(Employee employee);
	
	public List<ExpenseClaim> findByTechleadId(int id);
	
	public List<ExpenseClaim> findByProgressStatus(String status);

}
