package com.reimburser.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Employee;
import com.reimburser.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{

	public Collection<Project> findByEmployee(Employee employee);
}
