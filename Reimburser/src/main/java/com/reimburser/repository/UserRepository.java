package com.reimburser.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Employee;
import com.reimburser.model.User;
import com.reimburser.model.UserRole;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>, UserRepositoryCustom {

    public Optional<UserDetails> findByEmail(String email);
    public User findByEmployee(Employee employee);
    public Collection<User> findByUserRole(UserRole userRole);
    
   // public Employee findBy
}
