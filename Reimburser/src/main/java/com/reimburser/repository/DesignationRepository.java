package com.reimburser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reimburser.model.Designation;

@Repository
public interface DesignationRepository extends JpaRepository<Designation, Integer> {

}
