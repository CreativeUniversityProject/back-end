package com.reimburser.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Department {
	@Id
	private int departmentId;
	private String department;
	
	
	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Department(int departmentId, String department_name) {
		super();
		this.departmentId = departmentId;
		this.department = department_name;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartment_id(int departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL, orphanRemoval = true)
	 private List<Employee> employees;
	
}
