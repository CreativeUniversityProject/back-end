package com.reimburser.model;

import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class BankAccount {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "accountid_generator")
	@SequenceGenerator(name="accountid_generator", initialValue = 40000, allocationSize = 1,sequenceName = "acc_sequence")
	private int accountId;
	
	private BigInteger accountNo;
	private String bankName;
	private String branch;
	
	public int getAccountId() {
		return accountId;
	}

	public BigInteger getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(BigInteger accountNo) {
		this.accountNo = accountNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_id")
	private Employee employee;
	
	
	//medical claim
//	
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "bankAccount")
//	private Collection<MedicalClaim> medicalClaim= new ArrayList<>();
//	

}
