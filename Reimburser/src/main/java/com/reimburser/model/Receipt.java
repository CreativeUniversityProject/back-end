package com.reimburser.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
public class Receipt {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "receiptid_generator")
	@SequenceGenerator(name="receiptid_generator", initialValue = 9000, allocationSize = 1,sequenceName = "receipt_sequence")
	private int receiptId;
	//private Blob receipt;
	private Date date;
	private String description;
	private double amount;
	private String place;
	private String receiptImage;
	
	
	
	public String getReceiptImage() {
		return receiptImage;
	}


	public void setReceiptImage(String receiptImage) {
		this.receiptImage = receiptImage;
	}


	public String getPlace() {
		return place;
	}


	public void setPlace(String place) {
		this.place = place;
	}


	public int getReceiptId() {
		return receiptId;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "medical_form_id", referencedColumnName = "medicalFormId")
	@JsonIgnore
	private MedicalClaim medicalClaim;

	public MedicalClaim getMedicalClaim() {
		return medicalClaim;
	}

	public void setMedicalClaim(MedicalClaim medicalClaim) {
		this.medicalClaim = medicalClaim;
	}
	
	
	

}
