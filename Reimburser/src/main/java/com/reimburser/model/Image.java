package com.reimburser.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
public class Image {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "imageid_generator")
	@SequenceGenerator(name="imageid_generator", initialValue = 10000, allocationSize = 1,sequenceName = "image_sequence")
	private int imgId;
	//private Blob image;
	private Date date;
	private String description;
	private double amount;
	private String place;
	
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}

	private String billImage;
	
	
	
	
	public String getBillImage() {
		return billImage;
	}
	public void setBillImage(String billImage) {
		this.billImage = billImage;
	}
	public int getImgId() {
		return imgId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

//ExpenseClaim Relationship
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "expense_form_id", referencedColumnName = "expenseFormId")
	@JsonIgnore
	private ExpenseClaim expenseClaim;
	
	public ExpenseClaim getExpenseClaim() {
		return expenseClaim;
	}


	public void setExpenseClaim(ExpenseClaim expenseClaim) {
		this.expenseClaim = expenseClaim;
	}

}
