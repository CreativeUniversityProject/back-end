package com.reimburser.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Dependent {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "dependentid_generator")
	@SequenceGenerator(name="dependentid_generator", initialValue = 20000, allocationSize = 1,sequenceName = "dependent_sequence")
	private int dependentId;
	private String dependentName;
	private String relationship;
	private String gender;
	private Date dob;
	public String getDependentName() {
		return dependentName;
	}
	public void setDependentName(String dependentName) {
		this.dependentName = dependentName;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public int getDependentId() {
		return dependentId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_id", referencedColumnName = "employeeId")
		private Employee employee;
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	 @ManyToMany(fetch = FetchType.LAZY)
	  @JoinTable(name=" dependent_medical_claim", joinColumns = {@JoinColumn(name="dependent_id")},
	  inverseJoinColumns = {@JoinColumn(name="medical_form_id")}
			  )
	  @JsonIgnore
	private Collection<MedicalClaim> medicalClaim=new ArrayList<>();
	 
	public Collection<MedicalClaim> getMedicalClaim() {
		return medicalClaim;
	}
	public void setMedicalClaim(Collection<MedicalClaim> medicalClaim) {
		this.medicalClaim = medicalClaim;
	}



	
	

	
	
	
	
}
