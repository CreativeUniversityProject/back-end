package com.reimburser.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "empid_generator")
	@SequenceGenerator(name="empid_generator", initialValue = 1000, allocationSize = 1,sequenceName = "emp_sequence")
	private int employeeId;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNo;
	private Date datetime;
	private String address;
	private String nicNo;
	private String gender;
	
	
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNicNo() {
		return nicNo;
	}
	public void setNicNo(String nicNo) {
		this.nicNo = nicNo;
	}
	public Date getDatetime() {
		return datetime;
	}
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Employee() {
		
	}

	
	public int getEmployeeId() {
		return employeeId;
	}	

	public Employee(int employeeId, String firstName, String lastName, String email, String phoneNo, Date datetime,
		String address, String nicNo, String gender) {
	super();
	this.employeeId = employeeId;
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
	this.phoneNo = phoneNo;
	this.datetime = datetime;
	this.address = address;
	this.nicNo = nicNo;
	this.gender=gender;
}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + employeeId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId)
			return false;
		return true;
	}
	
	//Department Relationship
	@ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(foreignKey = @ForeignKey(name="department_id"), name = "deparment_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	 private Department department;

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	
	//Designation Relationship
	@ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(foreignKey = @ForeignKey(name="designation_id"), name = "designation_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	 private Designation designation;


	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}
	
	//Medical Claim relationship
	@OneToMany(mappedBy = "employee")
	 private List<MedicalClaim> medicalClaim;
	
	//Expense Claim relationship
		@OneToMany(mappedBy = "employee")
		 private List<ExpenseClaim> expenseClaim;



		  
		
		//Project relationship
		
		 @ManyToMany(fetch = FetchType.LAZY)
		  @JoinTable(name=" employee_projects", joinColumns = {@JoinColumn(name="employee_id")},
		  inverseJoinColumns = {@JoinColumn(name="project_id")}
				  )
		  @JsonIgnore
		 private Collection<Project> project=new ArrayList<>();


		public Collection<Project> getProject() {
			return project;
		}

		public void setProject(Collection<Project> project) {
			this.project = project;
		}
		
		//Dependent relationship
		
		@OneToMany(mappedBy = "employee")
		private Collection<Dependent> dependent=new ArrayList<Dependent>();
		
		
		//Bank Account
		@OneToOne(mappedBy = "employee")
		 private BankAccount bankAccount;
		
		//profile picture relationship
		@OneToOne
		@JoinColumn(name="profile_pic_id")
		@JsonIgnore
		private ProfilePictures profilePictures;
		public ProfilePictures getProfilePictures() {
			return profilePictures;
		}
		public void setProfilePictures(ProfilePictures profilePictures) {
			this.profilePictures = profilePictures;
		}
		
		@ManyToOne
		@JoinColumn(name="userrole_id")
		private UserRole userRole;

		public UserRole getUserRole() {
			return userRole;
		}
		public void setUserRole(UserRole userRole) {
			this.userRole = userRole;
		}
		
		
//		@OneToOne
//		@JoinColumn(name = "user_id", referencedColumnName = "id")
//		private User user;
//
//
//
//		public User getUser() {
//			return user;
//		}
//		public void setUser(User user) {
//			this.user = user;
//		}
//		
		
		
		

		  
	
}
