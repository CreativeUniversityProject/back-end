package com.reimburser.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
public class ExpenseClaim {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "expenseid_generator")
	@SequenceGenerator(name="expenseid_generator", initialValue = 12000, allocationSize = 1,sequenceName = "expense_sequence")
	private int expenseFormId; 
	 private Date submittedDate;
	  private double amount;
	  private String paymentMethod;
	 private String formDescription;
	 private String progressStatus;
	 private int techleadId;
	 
	 private String remark;
	 private double approvedAmount;
	 private String project;
	 
	 
	 
	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(double approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public int getTechleadId() {
		return techleadId;
	}

	public void setTechleadId(int techleadId) {
		this.techleadId = techleadId;
	}

	public int getExpenseFormId() {
		return expenseFormId;
	}
	
	public Date getSubmittedDate() {
		return submittedDate;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getFormDescription() {
		return formDescription;
	}
	public void setFormDescription(String formDescription) {
		this.formDescription = formDescription;
	}
	public String getProgressStatus() {
		return progressStatus;
	}
	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}
	public ExpenseClaim() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public ExpenseClaim(int expenseFormId, Date submittedDate, double amount, String paymentMethod,
			String formDescription, String progressStatus, int techleadId, String remark, double approvedAmount) {
		super();
		this.expenseFormId = expenseFormId;
		this.submittedDate = submittedDate;
		this.amount = amount;
		this.paymentMethod = paymentMethod;
		this.formDescription = formDescription;
		this.progressStatus = progressStatus;
		this.techleadId = techleadId;
		this.remark = remark;
		this.approvedAmount = approvedAmount;
	}


	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_id", referencedColumnName = "employeeId")
	 private Employee employee;



	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@OneToMany(mappedBy = "expenseClaim")
	private Collection<Image> image=new ArrayList<>();


	public Collection<Image> getImage() {
		return image;
	}

	public void setImage(Collection<Image> image) {
		this.image = image;
	}

	//review expense relationship
//	@OneToMany(mappedBy = "expenseClaim", cascade = CascadeType.ALL)
//	@JsonIgnore
//	private Collection<ReviewExpense> reviewExpense= new ArrayList<>();
//
//
//	public Collection<ReviewExpense> getReviewExpense() {
//		return reviewExpense;
//	}
//
//	public void setReviewExpense(Collection<ReviewExpense> reviewExpense) {
//		this.reviewExpense = reviewExpense;
//	}
	
	
	
	 
	 
}
