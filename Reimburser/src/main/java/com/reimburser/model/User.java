package com.reimburser.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class User implements UserDetails {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "userid_generator")
	@SequenceGenerator(name="userid_generator", initialValue = 100, allocationSize = 1,sequenceName = "user_sequence")
    private Integer id;
    private String email;
    
    private boolean enabled = true;

    @JsonProperty( value = "password", access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public Integer getId() {
        return id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

    public User() {
		super();
		// TODO Auto-generated constructor stub
	}
    
//    public Collection<UserRole> getuserRoles(User user){
//    	
//    	roles=user.getUserRole();
//    	return roles;
//    	
//    }


	

//	public Collection<UserRole> setRoles(Collection<UserRole> role) {
//		Collection<UserRole> roles = role;
//		return roles;
//	}


	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO Auto-generated method stub
    	 
           List<SimpleGrantedAuthority> authorities = new ArrayList<>(); 
            
       
               authorities.add(new SimpleGrantedAuthority(getUserRole().getRoleName()));
        
            
           return authorities;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
   
    @OneToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "employeeId")
   private Employee employee;

	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	// UserRole Relationship
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name="userrole_id")
	  private UserRole userRole;

	public UserRole getUserRole() {
		return userRole;
	}


	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}


	
}


