package com.reimburser.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
public class MedicalClaim {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "medicalid_generator")
	@SequenceGenerator(name="medicalid_generator", initialValue = 13000, allocationSize = 1,sequenceName = "medical_sequence")
	private int medicalFormId;
	private Date submittedDate;
	private double amount;
	private String paymentMethod;
	private int techleadId;
	private String formDescription;
	private String doctorName;
	private String diagnonsis;
	private String progressStatus;
	private Boolean self;
	

	 public Boolean getSelf() {
		return self;
	}

	public void setSelf(Boolean self) {
		this.self = self;
	}

	private String remark;
	 private double approvedAmount;
	 
	 
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(double approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public int getTechleadId() {
		return techleadId;
	}

	public void setTechleadId(int techleadId) {
		this.techleadId = techleadId;
	}

	public int getMedicalFormId() {
		return medicalFormId;
	}
	
	public Date getSubmittedDate() {
		return submittedDate;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getFormDescription() {
		return formDescription;
	}
	public void setFormDescription(String formDescription) {
		this.formDescription = formDescription;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getDiagnonsis() {
		return diagnonsis;
	}
	public void setDiagnonsis(String diagnonsis) {
		this.diagnonsis = diagnonsis;
	}
	public String getProgressStatus() {
		return progressStatus;
	}
	public void setProgressStatus(String progressStatus) {
		this.progressStatus = progressStatus;
	}
	
	
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_id", referencedColumnName = "employeeId")
	 private Employee employee;



	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public MedicalClaim() {
		super();
		// TODO Auto-generated constructor stub
	}


	public MedicalClaim(int medicalFormId, Date submittedDate, double amount, String paymentMethod, int techleadId,
			String formDescription, String doctorName, String diagnonsis, String progressStatus, String remark,
			double approvedAmount,boolean self) {
		super();
		this.medicalFormId = medicalFormId;
		this.submittedDate = submittedDate;
		this.amount = amount;
		this.paymentMethod = paymentMethod;
		this.techleadId = techleadId;
		this.formDescription = formDescription;
		this.doctorName = doctorName;
		this.diagnonsis = diagnonsis;
		this.progressStatus = progressStatus;
		this.remark = remark;
		this.approvedAmount = approvedAmount;
		this.self=self;
	}



	@OneToMany(mappedBy = "medicalClaim")
	private Collection<Receipt> receipt= new ArrayList<>();

	public Collection<Receipt> getReceipt() {
		return receipt;
	}

	public void setReceipt(Collection<Receipt> receipt) {
		this.receipt = receipt;
	}
	

	//Medical Claim relationship
	@ManyToMany(mappedBy = "medicalClaim")
	  private Collection<Dependent> dependent=new ArrayList<>();

	public Collection<Dependent> getDependent() {
		return dependent;
	}

	public void setDependent(Collection<Dependent> dependent) {
		this.dependent = dependent;
	}	
	
}
