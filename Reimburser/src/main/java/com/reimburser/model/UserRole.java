package com.reimburser.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class UserRole {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "roleid_generator")
	@SequenceGenerator(name="roleid_generator", initialValue = 50, allocationSize = 1,sequenceName = "role_sequence")
	private int roleId;
	private String roleName;
	
	public int getRoleId() {
		return roleId;
	}

	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
	
	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserRole(int roleId, String roleName) {
		super();
		this.roleId = roleId;
		this.roleName = roleName;
	}



	
	 @OneToMany(fetch = FetchType.EAGER)
	  @JoinTable(name=" user_have_roles", joinColumns = {@JoinColumn(name="role_id")},
	  inverseJoinColumns = {@JoinColumn(name="id")}
			  )
	  @JsonIgnore
	private Collection<User> user=new ArrayList<>();

	public Collection<User> getUser() {
		return user;
	}

	public void setUser(Collection<User> user) {
		this.user = user;
	}

	
	
}
