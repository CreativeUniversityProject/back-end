package com.reimburser.model.dto;

import java.util.Collection;

import com.reimburser.model.UserRole;

public class UserDto {
	private String userName;
	private String password;
	private int employeeId;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	private Collection<UserRole>  userRole;
	public Collection<UserRole> getUserRole() {
		return userRole;
	}
	public void setUserRole(Collection<UserRole> userRole) {
		this.userRole = userRole;
	}
	
	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserDto(String userName, String password, int employeeId, Collection<UserRole> userRole) {
		super();
		this.userName = userName;
		this.password = password;
		this.employeeId = employeeId;
		this.userRole = userRole;
	}
	
	
	
	
	
}
