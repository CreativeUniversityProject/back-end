package com.reimburser.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ProfilePictures {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "profilepicid_generator")
	@SequenceGenerator(name="profilepicid_generator", initialValue = 4000, allocationSize = 1,sequenceName = "profilepic_sequence")
	private int profilePicId;	
	private String profilePicture;
		
	@OneToOne
	@JoinColumn(name="employee_id")
	private Employee employee;

	public int getProfilePicId() {
		return profilePicId;
	}

	public void setProfilePicId(int profilePicId) {
		this.profilePicId = profilePicId;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	

}
