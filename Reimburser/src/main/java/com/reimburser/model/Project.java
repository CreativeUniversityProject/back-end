package com.reimburser.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;


import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToMany;

import javax.persistence.SequenceGenerator;
@Entity
public class Project {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "projectid_generator")
	@SequenceGenerator(name="projectid_generator", initialValue = 8000, allocationSize = 1,sequenceName = "project_sequence")
	private int projectId;
	
	private String projectName;
	private Date startDate;
	private Date endDate;
	
	
	public Project() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Project(int projectId, String projectName, Date startDate, Date endDate) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.startDate = startDate;
		this.endDate = endDate;
	
	}
	

	
	public int getProjectId() {
		return projectId;
	}



	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}



	@ManyToMany(mappedBy = "project")
	 private Collection<Employee> employee=new ArrayList<>();


	public Collection<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(Collection<Employee> employee) {
		this.employee = employee;
	}




	
	

	
}
